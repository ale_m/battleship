import React, { Component } from 'react';

import './App.css';
import { Game, INITIAL_SHIPS } from './Game';

export default class App extends Component {
    constructor(props, context) {
        super(props, context);

        this._initGame();

        this.state = {
            ...this.game.state,
            autoShot: false
        };
    }

    componentDidUpdate() {
        if (this.state.autoShot && !this.isEnd) {
            this.timeout = setTimeout(this.handleFireClick, 50);
        }
    }

    _initGame() {
        this.game = new Game(10, 10, INITIAL_SHIPS);
    }

    handleStopClick = () => {
        clearTimeout(this.timeout);

        this.setState({ autoShot: false });
    };

    handleFireClick = () => {
        this.game.makeShot();

        this.setState({
            ...this.game.state,
            autoShot: true
        });
    };

    handleRetryButtonClick = () => {
        this._initGame();

        this.setState({
            ...this.game.state,
            autoShot: false
        });
    };

    get isEnd() {
        return !this.state.shipsHealth || !this.state.shots.length;
    }

    render() {
        const btn = this.state.autoShot ?
            <button className="app__button" onClick={this.handleStopClick}>Stop!</button> :
            <button className="app__button" onClick={this.handleFireClick}>Fire!</button>;
        const alarm = <div className="app__alarm">
            <span>The game is over! </span>
            <button onClick={this.handleRetryButtonClick}>Retry</button>
        </div>;

        return (
            <div className="app">
                {this.isEnd ? alarm : btn}
                <div className="app__field">
                    {this.state.field.map((row, i) => (
                        <div className="app__row" key={i}>
                            {row.map((state, j) =>
                                <div className={`app__cell app__cell_${state}`} key={`${i}${j}`}/>
                            )}
                        </div>))
                    }
                </div>
            </div>
        );
    }
}
