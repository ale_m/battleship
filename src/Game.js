export class Game {
    constructor(rows = 10, columns = 10, ships = []) {
        this.rows = rows;
        this.columns = columns;
        this.shipsHealth = 0;

        this._initFieldAndShots(rows, columns);
        this._initShips(ships);
    }

    makeShot() {
        const index = Game.getRandomInt(this.shots.length - 1);
        const shot = this.shots[index];

        if (this.field[shot.row][shot.column] === Game.STATE_DECK) {
            this.shipsHealth--;
        }

        this.field[shot.row][shot.column] += 10;

        this.shots.splice(index, 1);
    }

    get state() {
        return {
            field: this.field,
            shots: this.shots,
            shipsHealth: this.shipsHealth
        };
    }

    _initFieldAndShots() {
        this.field = [];
        this.shots = [];

        for (let row = 0; row < this.rows; row++) {
            this.field[row] = [];

            for (let column = 0; column < this.columns; column++) {
                this.field[row][column] = Game.STATE_WATER;
                this.shots.push({
                    row,
                    column
                });
            }
        }
    }

    _initShips(ships = []) {
        ships.forEach(ship => {
            let count = ship.count;

            while (count) {
                this._createShip(ship);

                count--;
            }
        });
    }

    _createShip(ship) {
        const scheme = Game.getSchemeByOrientation(ship.scheme);
        const maxPosition = {
            row: scheme.length - 1,
            column: scheme[0].length - 1
        };

        while (true) {
            const startPosition = Game.getRandomPosition(this.columns, this.rows);
            const positions = this._getShipPositions(startPosition, maxPosition, scheme);

            if (positions) {
                this._placeShip(positions);

                break;
            }
        }

    }

    _getShipPositions(startPosition, maxPosition, scheme) {
        const positions = [];

        for (let i = 0; i <= maxPosition.row; i++) {
            for (let j = 0; j <= maxPosition.column; j++) {
                const position = {
                    row: startPosition.row + i,
                    column: startPosition.column + j,
                    state: scheme[i][j]
                };

                if (this._isBusyPosition(position.row, position.column)) {
                    return;
                }

                positions.push(position);
            }
        }

        return positions;
    }

    _placeShip(positions) {
        positions.forEach(position => {
            if (position.state === Game.STATE_DECK) {
                this.field[position.row][position.column] = position.state;
                this.shipsHealth++;

                this._markBufferArea(position.row, position.column);
            }
        });
    };

    _markBufferArea(row, column) {
        for (let i = row - 1; i <= row + 1; i++) {
            if (!this.field[i]) {
                continue;
            }

            for (let j = column - 1; j <= column + 1; j++) {
                if (this.field[i][j] === Game.STATE_WATER) {
                    this.field[i][j] = Game.STATE_BUFFER;
                }
            }
        }
    }

    _isBusyPosition(row, column) {
        return !this.field[row] || this.field[row][column] !== Game.STATE_WATER;
    }

    static STATE_WATER = 0;
    static STATE_DECK = 1;
    static STATE_BUFFER = 2;

    static getSchemeByOrientation(scheme) {
        const newScheme = Game.getRandomInt() >= 5 ?
            scheme : scheme[0].map((col, i) => scheme.map(row => row[i]));

        if (scheme.length === 1) {
            return newScheme;
        }

        if (Game.getRandomInt() >= 5) {
            newScheme.reverse();
        }

        if (Game.getRandomInt() >= 5) {
            newScheme.map(col => col.reverse());
        }

        return newScheme;
    }

    static getRandomPosition(maxColumn, maxRow, min = 0) {
        return {
            column: this.getRandomInt(maxColumn, min),
            row: this.getRandomInt(maxRow, min)
        };
    }

    static getRandomInt(max = 9, min = 0) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}

export const INITIAL_SHIPS = [
    {
        count: 1,
        scheme: [
            [0, 0, 1],
            [1, 1, 1]
        ]
    },
    {
        count: 1,
        scheme: [
            [1, 1, 1, 1]
        ]
    },
    {
        count: 2,
        scheme: [
            [1]
        ]
    }
];
